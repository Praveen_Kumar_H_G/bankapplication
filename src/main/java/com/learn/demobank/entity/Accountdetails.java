package com.learn.demobank.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Accountdetails {
	@Id
	private String accountId;
	private long accountNumber;
	private String ifsc;
	private double balance;
	private String accountType;
	private String bankName;
	@OneToOne 	
	private CustomerRegistration customer;

}
