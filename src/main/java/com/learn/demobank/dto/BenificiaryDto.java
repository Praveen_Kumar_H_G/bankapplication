package com.learn.demobank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BenificiaryDto {
	@NotNull(message = "accountNumber is required")
	private long accountNumber;
	@NotBlank(message = "ifsc is required")
	private String ifsc;
	@NotBlank(message = "bankName is required")
	private String bankName;
	@NotBlank(message = "accountHolderName is required")
	private String accountHolderName;

}
